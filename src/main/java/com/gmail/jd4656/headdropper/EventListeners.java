package com.gmail.jd4656.headdropper;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Skull;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.google.common.base.Preconditions.checkArgument;

public class EventListeners implements Listener {
    private HeadDropper plugin;

    EventListeners(HeadDropper p) {
        plugin = p;
    }

    private String getDayOfMonthSuffix(final int n) {
        checkArgument(n >= 1 && n <= 31, "illegal day of month: " + n);
        if (n >= 11 && n <= 13) {
            return "th";
        }
        switch (n % 10) {
            case 1:  return "st";
            case 2:  return "nd";
            case 3:  return "rd";
            default: return "th";
        }
    }

    @EventHandler (priority= EventPriority.LOWEST)
    public void PlayerDeathEvent(PlayerDeathEvent event) {
        Player player = event.getEntity();
        Player killer = player.getKiller();
        if (killer == null) return;
        if (!killer.hasPermission("headdropper.behead")) return;
        
        ItemStack playerHead = new ItemStack(Material.PLAYER_HEAD, 1);
        ItemMeta meta = playerHead.getItemMeta();

        List<String> lore = new ArrayList<>();
        Date date = new Date();

        String timestamp = new SimpleDateFormat("MMMM").format(date);
        try {
            String num = new SimpleDateFormat("d").format(date);
            timestamp += " " + num + getDayOfMonthSuffix(Integer.parseInt(num));
        } catch (NumberFormatException ignored) {}
        timestamp += " " + new SimpleDateFormat("yyyy").format(date);

        if (event.getDeathMessage().equals(player.getName() + " fell from a high place")) {
            lore.add(ChatColor.YELLOW + player.getName() + " was knocked off a cliff by " + killer.getName());
        } else {
            lore.add(ChatColor.YELLOW + event.getDeathMessage());
        }
        Material weaponType = event.getEntity().getKiller().getInventory().getItemInMainHand().getType();
        lore.add(ChatColor.YELLOW + "Killed with: " + ChatColor.GREEN + plugin.materialName(weaponType));
        lore.add(ChatColor.GRAY + "Date: " + timestamp);
        meta.setLore(lore);
        meta.setDisplayName(ChatColor.RED + player.getName() + "'s head.");
        SkullMeta skullMeta = (SkullMeta) meta;
        skullMeta.setOwningPlayer(player);

        playerHead.setItemMeta(skullMeta);
        player.getWorld().dropItemNaturally(player.getLocation(), playerHead);
    }

    @EventHandler
    public void BlockPlaceEvent(BlockPlaceEvent event) {
        Block placedBlock = event.getBlockPlaced();
        ItemStack itemHead = event.getItemInHand();

        if (itemHead == null || !itemHead.hasItemMeta() || !itemHead.getType().equals(Material.PLAYER_HEAD)) return;
        ItemMeta meta = itemHead.getItemMeta();
        if (!meta.hasLore()) return;

        ItemStack clonedHead = itemHead.clone();
        clonedHead.setAmount(1);

        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

            dataOutput.writeObject(clonedHead);
            dataOutput.close();

            Connection c = plugin.getConnection();
            PreparedStatement pstmt = c.prepareStatement("INSERT INTO heads (world, x, y, z, item) VALUES (?, ?, ?, ?, ?)");
            pstmt.setString(1, placedBlock.getWorld().getName());
            pstmt.setInt(2, placedBlock.getLocation().getBlockX());
            pstmt.setInt(3, placedBlock.getLocation().getBlockY());
            pstmt.setInt(4, placedBlock.getLocation().getBlockZ());
            pstmt.setString(5, Base64Coder.encodeLines(outputStream.toByteArray()));
            pstmt.executeUpdate();
        } catch (Exception e) {
            plugin.getLogger().severe("BlockPlaceEvent: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
        }
    }

    @EventHandler  (priority=EventPriority.HIGHEST)
    public void BlockBreakEvent(BlockBreakEvent event) {
        Player player = event.getPlayer();
        Block block = event.getBlock();
        if (event.isCancelled()) return;
        if (!(block.getType().equals(Material.PLAYER_HEAD) || block.getType().equals(Material.PLAYER_WALL_HEAD)) || player == null || !(block.getState() instanceof Skull)) return;
        Skull head = (Skull) block.getState();

        try {
            Connection c = plugin.getConnection();
            PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM heads WHERE world=? AND x=? AND y=? AND z=?");
            pstmt.setString(1, block.getWorld().getName());
            pstmt.setInt(2, block.getLocation().getBlockX());
            pstmt.setInt(3, block.getLocation().getBlockY());
            pstmt.setInt(4, block.getLocation().getBlockZ());
            ResultSet rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") < 1) {
                pstmt.close();
                return;
            }

            pstmt = c.prepareStatement("SELECT * FROM heads WHERE world=? AND x=? AND y=? AND z=?");
            pstmt.setString(1, block.getWorld().getName());
            pstmt.setInt(2, block.getLocation().getBlockX());
            pstmt.setInt(3, block.getLocation().getBlockY());
            pstmt.setInt(4, block.getLocation().getBlockZ());
            rs = pstmt.executeQuery();

            String deathMessage = rs.getString("lore");
            String itemStr = rs.getString("item");

            pstmt = c.prepareStatement("DELETE FROM heads WHERE world=? AND x=? AND y=? AND z=?");
            pstmt.setString(1, block.getWorld().getName());
            pstmt.setInt(2, block.getLocation().getBlockX());
            pstmt.setInt(3, block.getLocation().getBlockY());
            pstmt.setInt(4, block.getLocation().getBlockZ());
            pstmt.executeUpdate();
            pstmt.close();

            ItemStack playerHead;

            if (deathMessage != null) {
                playerHead = new ItemStack(Material.PLAYER_HEAD, 1);
                ItemMeta meta = playerHead.getItemMeta();
                List<String> lore = new ArrayList<>();
                lore.add(deathMessage.trim());
                meta.setLore(lore);
                meta.setDisplayName(ChatColor.RESET + head.getOwningPlayer().getName() + "'s head.");
                SkullMeta skullMeta = (SkullMeta) meta;
                skullMeta.setOwningPlayer(head.getOwningPlayer());
                playerHead.setItemMeta(skullMeta);
            } else {
                ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(itemStr));
                BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
                playerHead = (ItemStack) dataInput.readObject();
                dataInput.close();
            }

            playerHead.setAmount(1);
            player.getWorld().dropItemNaturally(player.getLocation(), playerHead);
            event.setDropItems(false);
        } catch (Exception e) {
            plugin.getLogger().severe("BlockBreakEvent: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
        }
    }

    @EventHandler
    public void PlayerInteractEvent(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Action action = event.getAction();
        Block block = event.getClickedBlock();

        if (!action.equals(Action.RIGHT_CLICK_BLOCK) || block == null || !(block.getType().equals(Material.PLAYER_WALL_HEAD) || block.getType().equals(Material.PLAYER_HEAD)) || !event.getHand().equals(EquipmentSlot.HAND)) return;

        try {
            Connection c = plugin.getConnection();
            PreparedStatement pstmt = c.prepareStatement("SELECT count(*) FROM heads WHERE world=? AND x=? AND y=? AND z=?");
            pstmt.setString(1, block.getWorld().getName());
            pstmt.setInt(2, block.getLocation().getBlockX());
            pstmt.setInt(3, block.getLocation().getBlockY());
            pstmt.setInt(4, block.getLocation().getBlockZ());
            ResultSet rs = pstmt.executeQuery();

            if (rs.getInt("count(*)") < 1) {
                pstmt.close();
                return;
            }

            pstmt = c.prepareStatement("SELECT * FROM heads WHERE world=? AND x=? AND y=? AND z=?");
            pstmt.setString(1, block.getWorld().getName());
            pstmt.setInt(2, block.getLocation().getBlockX());
            pstmt.setInt(3, block.getLocation().getBlockY());
            pstmt.setInt(4, block.getLocation().getBlockZ());
            rs = pstmt.executeQuery();

            String loreLine = rs.getString("lore");
            String itemStr = rs.getString("item");
            pstmt.close();

            if (loreLine != null) {
                player.sendMessage(ChatColor.DARK_RED + "[Heads] " + loreLine);
            } else {
                ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(itemStr));
                BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
                ItemStack playerHead = (ItemStack) dataInput.readObject();
                ItemMeta meta = playerHead.getItemMeta();
                List<String> lore = meta.getLore();

                for (String itemLore : lore) player.sendMessage(ChatColor.DARK_RED + "[Heads] " + itemLore);
            }
        } catch (Exception e) {
            plugin.getLogger().severe("PlayerInteractEvent: " + e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
        }
    }
}
