package com.gmail.jd4656.headdropper;

import org.bukkit.Material;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class HeadDropper extends JavaPlugin {
    HeadDropper plugin;
    private File dbFile;
    private Connection conn = null;

    @Override
    public void onEnable() {
        plugin = this;
        plugin.getDataFolder().mkdirs();

        dbFile = new File(plugin.getDataFolder(), "Heads.db");
        if (!dbFile.exists()) {
            try {
                dbFile.createNewFile();
            } catch (IOException e) {
                plugin.getLogger().severe("File write error: Heads.db");
                return;
            }
        }

        getServer().getPluginManager().registerEvents(new EventListeners(this), this);

        try {
            Statement stmt;
            Connection c = this.getConnection();

            stmt = c.createStatement();
            stmt.executeUpdate("CREATE TABLE IF NOT EXISTS heads (lore TEXT, world TEXT, x INTEGER, y INTEGER, z INTEGER, item TEXT)");

            stmt.close();
        } catch (Exception e) {
            plugin.getLogger().severe(e.getClass().getName() + ": " + e.getMessage());
            e.printStackTrace();
            plugin.getLogger().warning("HeadDropper failed to load.");
            return;
        }

        plugin.getLogger().info("HeadDropper loaded.");
    }

    public void onDisable() {
        if (conn != null) {
            try {
                conn.close();
            } catch (Exception ignored) {}
        }
    }

    Connection getConnection() throws Exception {
        if (conn == null) {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + dbFile);
        }
        return conn;
    }

    String materialName(Material material) {
        String name = material.name().replace("_", " ");
        String[] split = name.split(" ");

        for (int i=0; i<split.length; i++) {
            split[i] = Character.toUpperCase(split[i].charAt(0)) + split[i].substring(1).toLowerCase();
        }

        return String.join(" ", split);
    }
}
